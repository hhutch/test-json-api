class ApplicationController < ActionController::API
  # before_filter :authenticate_user!

  include ActionController::MimeResponds
  include ActionController::HttpAuthentication::Token::ControllerMethods
end
