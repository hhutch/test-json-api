class Api::V1::InvitationsController < Devise::InvitationsController
  before_filter :restrict_access
  
  # def update
  #   if this
  #     redirect_to root_path
  #   else
  #     super
  #   end
  # end

  protected
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      User.exists?(authentication_token: token)
    end
  end
end
