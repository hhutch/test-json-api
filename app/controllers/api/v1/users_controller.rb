class Api::V1::UsersController < ApplicationController
  before_filter :restrict_access

  def index
    @users = User.all
    
    render json: @users
  end

  def show
    @users = User.all

    render json: @users
  end

  # def update
  #   @user = User.find(params[:id])
  #   render json: @user
  # end
  
  protected

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      User.exists?(authentication_token: token)
    end
  end
end
