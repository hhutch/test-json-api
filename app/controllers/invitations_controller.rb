class InvitationsController < Devise::InvitationsController
  # prepend_before_filter :require_no_authentication
  before_filter :restrict_access

  respond_to :json
  # skip_before_filter :verify_authenticity_token

  def create
    super
  end
  
  # def update
  #   if this
  #     redirect_to root_path
  #   else
  #     super
  #   end
  # end

  # respond_to :json
  
  # def new
  #   super
  # end

  # private
  # def resource_params
  #   params.permit(user: [:name, :email,:invitation_token, :your_params_here])[:user]
  # end
  
  protected
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      User.exists?(authentication_token: token)
    end
  end
end
