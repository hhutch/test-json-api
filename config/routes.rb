TestJsonApi::Application.routes.draw do
  # resources :users
  resources :tasks, except: [:new, :edit]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  namespace :api do
    namespace :v1 do
      devise_for(:users,
                 :controllers => {
                   :invitations => "api/v1/invitations",
                   # :invitations => "invitations",
                   :sessions => "api/v1/sessions",
                   :registrations => "api/v1/registration"})


      devise_scope :user do
        post 'sessions' => 'sessions#create', :as => 'login'
        delete 'sessions' => 'sessions#destroy', :as => 'logout'
      end
      resources :tasks, except: [:new, :edit]
      resources :users
    end
  end
  
end
